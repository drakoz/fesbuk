from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import index
from .models import Message
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class vLineUnitTest(TestCase):
    def test_vline_url_is_exist(self):
        response = Client().get('/vline/')
        self.assertEqual(response.status_code, 200)

    def test_vline_using_index_func(self):
        found = resolve('/vline/')
        self.assertEqual(found.func, index)

    def test_message(self):
        new = Message.objects.create(name='johnny', message='saya butuh teman')
        count = Message.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(new.__str__(), "johnny")

    def test_color_change(self):
        new = Message.objects.create(name='johnny', message='saya butuh teman', bgcolor='R')
        response = Client().post(reverse("vline:index"), {
            "objectid" : new.id,
            "bgcolor" : "G",
        })
        aftercolorchange = Message.objects.get(id=new.id)
        self.assertEqual(aftercolorchange.bgcolor, "G")
        self.assertNotEqual(aftercolorchange.bgcolor, "R")

    def test_color_random(self):
        new = Message.objects.create(name='johnny', message='saya butuh teman', bgcolor='R')
        response = Client().post(reverse("vline:index"), {
            "objectid" : new.id,
            "bgcolor" : "Random",
        })
        aftercolorchange = Message.objects.get(id=new.id)
        self.assertNotEqual(aftercolorchange.bgcolor, "R")
    
    def test_message_GET(self):
        response = Client().get(reverse("vline:index"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "messages.html")

    def test_message_POST(self):
        response = Client().post(reverse("vline:addmessage"), {
            "name" : "johnny",
            "message" : "saya butuh teman",
            "bgcolor" : "R",
        })
        invalid_form = Client().post(reverse("vline:addmessage"), {
            "name" : "pengguna",
        })
        count = Message.objects.all().count()
        getadd = Client().get(reverse("vline:addmessage"))
        getlist = Client().get(reverse("vline:index"))
        self.assertEqual(count,1)
        self.assertNotContains(getlist, "pengguna")
        self.assertContains(getlist, "johnny")
        self.assertContains(getlist, "saya butuh teman")
        self.assertNotContains(getlist, "pesanku")
        self.assertTemplateUsed(getadd,"addmessage.html")

class vLineFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(vLineFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(vLineFunctionalTest, self).tearDown()

    def test_input_message(self):
        selenium = self.selenium
        # selenium.get('http://localhost:8080/vline/addmessage')
        selenium.get('https://fesbukpepew.herokuapp.com/vline/addmessage')
        name = selenium.find_element_by_id('id_nama')
        message = selenium.find_element_by_id('id_pesan')
        submit = selenium.find_element_by_id('submit')

        name.send_keys('nama saya')
        message.send_keys('pesan saya')
        submit.send_keys(Keys.RETURN)

    def test_change_color(self):
        selenium = self.selenium
        # selenium.get('http://localhost:8080/vline/')
        selenium.get('https://fesbukpepew.herokuapp.com/vline/')
        button = selenium.find_element_by_name('bgcolor')

        button.click()