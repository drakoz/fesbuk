from django.urls import path
from .views import index, addmessage

app_name = 'vline'

urlpatterns = [
    path('', index, name='index'),
    path('addmessage', addmessage, name='addmessage')
]