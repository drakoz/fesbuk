from django.shortcuts import render, redirect
from .models import Message
from .forms import Message_Form
from random import choice

# Create your views here.
def index(request):
    if request.method == 'POST':
        query = request.POST['objectid']
        item = Message.objects.get(id=query)
        querycolor = request.POST['bgcolor']
        if querycolor == 'Random':
            item.bgcolor = choice("RGBY".replace(item.bgcolor, ""))
            item.save()
            return redirect('/vline')
        item.bgcolor = querycolor
        item.save()
        return redirect('/vline')
    items = Message.objects.all()
    context = {'items':items}
    return render(request, 'messages.html', context)

def addmessage(request):
    if request.method == 'POST':
        form = Message_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/vline')
        else:
            return redirect('/vline')
    else:
        return render(request, 'addmessage.html')