from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
class Message(models.Model):
    class BGColor(models.TextChoices):
        RED = 'R', _('Red')
        GREEN = 'G', _('Green')
        BLUE = 'B', _('Blue')
        YELLOW = 'Y', _('Yellow')
        DEFAULT = 'D', _('Default')
    
    bgcolor = models.CharField(max_length=1, choices=BGColor.choices, default=BGColor.DEFAULT)
    name = models.CharField(max_length=40)
    message = models.TextField()

    def __str__(self):
        return self.name