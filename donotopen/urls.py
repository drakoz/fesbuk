from django.urls import path
from .views import index, register
from django.contrib.auth import views as auth_views

app_name = 'donotopen'

urlpatterns = [
    path('', index, name='index'),
    path('register/', register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='donotopen/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='donotopen/logout.html'), name='logout'),
]