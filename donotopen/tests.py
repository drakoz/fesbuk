from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import register
from django.contrib.auth.models import User
from django.contrib.auth import views as auth_views

class AuthUnitTest(TestCase):
    def test_auth_register_is_exist(self):
        response = Client().get('/donotopen/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_auth_using_register_func(self):
        found = resolve('/donotopen/register/')
        self.assertEqual(found.func, register)

    def test_auth_register_post(self):
        response = Client().post('/donotopen/register/', {'username':'ngadmin', 
        'password1': 'awokawok', 'password2': 'awokawok',})
        count = User.objects.all().count()
        self.assertTemplateUsed('login.html')
        self.assertEqual(count,1)

    def test_auth_register_post_wrong_confirmation_password(self):
        response = Client().post('/donotopen/register/', {'username':'ngadmin', 
        'password1': 'password123', 'password2': 'senangpepew',})
        count = User.objects.all().count()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(count,0)
    
    def test_auth_login_is_exist(self):
        response = Client().get('/donotopen/login/')
        self.assertEqual(response.status_code, 200)

    def test_auth_login_post(self):
        response = Client().post('/donotopen/login/', {'username': 'ngadmin', 'password':'password123'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('index.html')

    def test_auth_login_post_wrong_password(self):
        response = Client().post('/donotopen/login/', {'username': 'ngadmin', 'password':'pipiw'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('login.html')

    def test_auth_model(self):
        User.objects.create(username='thisisthesameasshopepewunittest', password ='justcopas')
        count = User.objects.all().count()
        self.assertEqual(count,1)
