from django.apps import AppConfig


class DonotopenConfig(AppConfig):
    name = 'donotopen'
