from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserCustom(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.CharField(default='http://getdrawings.com/img/facebook-profile-picture-silhouette-female-18.jpg', max_length=500)

    def __str__(self):
        return self.user.username