from django.urls import path
from .views import index

app_name = 'akurdion'

urlpatterns = [
    path('', index, name='index'),
]