from django.db import models

class Content(models.Model):
    title = models.CharField(max_length=60)
    content = models.TextField()

    def __str__(self):
        return self.title