from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .views import index
from .models import Content
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class AkurdionUnitTest(TestCase):
    def test_akudion_url_is_exist(self):
        response = Client().get('/akurdion/')
        self.assertEqual(response.status_code, 200)

    def test_akurdion_using_index_func(self):
        found = resolve('/akurdion/')
        self.assertEqual(found.func, index)

    def test_content(self):
        new = Content.objects.create(title='jangan dibuka', content='dibilang jangan')
        count = Content.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(new.__str__(), "jangan dibuka")

class AkurdionFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(AkurdionFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(AkurdionFunctionalTest, self).tearDown()

    def test_click_accordion(self):
        selenium = self.selenium
        # selenium.get('http://localhost:8080/akurdion/')
        selenium.get('https://fesbukpepew.herokuapp.com/akurdion/')
        accordion = selenium.find_element_by_id('button1')
        accordion.click()
        element = selenium.find_element_by_id('collapse1')
        self.assertNotEqual(element.get_attribute("class"), "collapse");

    def test_change_theme(self):
        selenium = self.selenium
        # selenium.get('http://localhost:8080/akurdion/')
        selenium.get('https://fesbukpepew.herokuapp.com/akurdion/')
        button = selenium.find_element_by_id('switch')
        button.click()
        element = selenium.find_element_by_tag_name('body')
        self.assertEqual(element.get_attribute("class"), "bg-dark")
        button.click()
        self.assertEqual(element.get_attribute("class"), "bg-light")