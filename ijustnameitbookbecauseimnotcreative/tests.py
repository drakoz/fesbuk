from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import index
from .models import Book

# Create your tests here.
class BookTest(TestCase):
    def test_book_url_is_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_book_using_index_func(self):
        found = resolve('/book/')
        self.assertEqual(found.func, index)

    def test_book(self):
        new = Book.objects.create(title='mein kampf', author='adolf hitler', likes=100)
        count = Book.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(new.__str__(), "mein kampf")