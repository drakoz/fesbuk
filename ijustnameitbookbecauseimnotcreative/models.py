from django.db import models

# Create your models here.
class Book(models.Model):
    id = models.CharField(max_length=100, primary_key=True, unique=True)
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    likes = models.IntegerField(default=0)

    class Meta:
        ordering = ['-likes']

    def __str__(self):
        return self.title