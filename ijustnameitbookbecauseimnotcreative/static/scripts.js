$(document).ready(function(){
    $('#searchbar').submit(function(){
        var search = $('#search').val();
        $('#search').attr('value',search);
        $('#result').empty();

        $.ajax({
            url:"https://www.googleapis.com/books/v1/volumes?q=" + search,
            dataType: "json",
            success: function(response){
                $('#result').css("display","flex");
                for (i=0; i<response.items.length; i++) {
                    $('#result').append('<div class="card d-flex justify-content-between align-items-center my-2 mx-auto" style="width:20rem;">' 
                    + '<a href=' + response.items[i].volumeInfo.infoLink + ' target="_blank"><img class="card-img-top" style="width:100%;" src=' + response.items[i].volumeInfo.imageLinks.thumbnail + '></a>'
                    + '<div class="card-body">' 
                    + '<h5 class="card-title">'+ response.items[i].volumeInfo.title + '</h5>'
                    + '<p class="card-text">' + response.items[i].volumeInfo.authors + '</p>' 
                    + '</div></div>');
                }
            },
            type: "GET"
        });
    });
});